const http = require('http')

const port = 3000;

const myServer = http.createServer((req, res) => {
	if (req.url == '/login') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('You are in the login page!')
	} else {
		res.writeHead(404, {'Content-Type': 'text/plain'})
		res.end('Page not Found!')
	}
});

myServer.listen(port);

console.log(`Server is currently running with the local host of ${port}.`)
